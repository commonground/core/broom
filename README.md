# Broom

A Kubernetes mutating webhook that keeps a cluster clean.


## How it works

1. When a namespace is created, it is annotated with a clean up date;
2. Periodicaly a clean up job runs to delete all namespaces that are due.
