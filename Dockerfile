FROM golang:1.19.0-alpine AS build

WORKDIR /go/src/broom

COPY go.mod go.sum ./
COPY vendor vendor
COPY cmd cmd
COPY pkg pkg

RUN go install -v ./cmd/broom
RUN addgroup -S -g 1001 broom && adduser -S -D -H -G broom -u 1001 broom


FROM alpine:3.16.2

COPY --from=build /go/bin/broom /usr/local/bin/broom
COPY --from=build /etc/passwd /etc/group /etc/

USER broom
ENTRYPOINT ["/usr/local/bin/broom"]
