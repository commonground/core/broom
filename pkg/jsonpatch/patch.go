package jsonpatch

import (
	"encoding/json"

	"k8s.io/klog/v2"
)

type PatchOp struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

type Patch []PatchOp

func (j *Patch) Add(op, path string, value interface{}) {
	o := PatchOp{op, path, value}
	*j = append(*j, o)
}

func (j *Patch) JSON() []byte {
	bytes, err := json.Marshal(j)
	if err != nil {
		klog.Errorf("Failed to marshal to JSON: %v", err)
		return nil
	}

	return bytes
}
