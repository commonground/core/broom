package util

import "strings"

type StringList []string

func (l StringList) Contains(str string) bool {
	for _, s := range l {
		if s == str {
			return true
		}
	}
	return false
}

func (l StringList) String() string {
	return strings.Join(l, ", ")
}
