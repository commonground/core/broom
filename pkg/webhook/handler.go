package webhook

import (
	"encoding/json"
	"io"
	"net/http"

	admissionv1 "k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/klog/v2"
)

var (
	scheme  = runtime.NewScheme()
	factory = serializer.NewCodecFactory(scheme)
	decoder = factory.UniversalDecoder()
)

type webhookHandler func(r *admissionv1.AdmissionReview) (*admissionv1.AdmissionReview, error)

func wrapHandler(handler webhookHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if contentType := r.Header.Get("Content-Type"); contentType != "application/json" {
			klog.Errorf("Unexpected content type: %s", contentType)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			klog.Errorf("Failed to read body: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		w.Header().Set("Content-Type", "application/json")

		requestReview := admissionv1.AdmissionReview{}

		if _, _, err := decoder.Decode(body, nil, &requestReview); err != nil {
			klog.Errorf("Failed to decode body: %v", err)
			writeErr(w, err)
			return
		}

		response, err := handler(&requestReview)
		if err != nil {
			writeErr(w, err)
			return
		}

		writeReview(w, response)
	})
}

func writeReview(w http.ResponseWriter, review *admissionv1.AdmissionReview) {
	bytes, err := json.Marshal(review)
	if err != nil {
		klog.Error(err)
	}

	if _, err := w.Write(bytes); err != nil {
		klog.Errorf("failed to write review: %v", err)
	}
}

func writeErr(w http.ResponseWriter, err error) {
	writeReview(w, &admissionv1.AdmissionReview{
		Response: &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		},
	})
}
