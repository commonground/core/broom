package webhook

import (
	"time"

	admissionv1 "k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/jsonpatch"
	"gitlab.com/commonground/core/broom/pkg/kubernetes"
	"gitlab.com/commonground/core/broom/pkg/util"
)

func newNamespacesHandler(duration time.Duration, usernames util.StringList) namespacesHandler {
	var filterFunc UsernameFilterFunc

	if len(usernames) == 0 {
		klog.Info("Will handle for all usernames")
		filterFunc = NoopFilter()
	} else {
		klog.Info("Will handle only for usernames: ", usernames)
		filterFunc = UsernameFilter(usernames)
	}

	return namespacesHandler{
		duration:           duration,
		usernameFilterFunc: filterFunc,
	}
}

type namespacesHandler struct {
	duration           time.Duration
	usernameFilterFunc UsernameFilterFunc
}

func (h namespacesHandler) Handler(review *admissionv1.AdmissionReview) (*admissionv1.AdmissionReview, error) {
	response := &admissionv1.AdmissionResponse{UID: review.Request.UID, Allowed: true}

	if h.shouldHandleRequest(review.Request) {
		annotationValue := time.Now().Add(h.duration).Format(AnnotationFormat)

		klog.Infof("Annotating namespace '%s' with '%s=%s'", review.Request.Name, AnnotationKey, annotationValue)

		patch := jsonpatch.Patch{}
		patch.Add(
			"add",
			"/metadata/annotations",
			map[string]string{
				AnnotationKey: annotationValue,
			},
		)

		response.Patch = patch.JSON()
		response.PatchType = func() *admissionv1.PatchType {
			pt := admissionv1.PatchTypeJSONPatch
			return &pt
		}()
	}

	return &admissionv1.AdmissionReview{
		TypeMeta: metav1.TypeMeta{
			Kind:       review.Kind,
			APIVersion: review.APIVersion,
		},
		Response: response,
	}, nil
}

func (h namespacesHandler) shouldHandleRequest(request *admissionv1.AdmissionRequest) bool {
	return request.Kind.Kind == "Namespace" &&
		request.Operation == admissionv1.Create &&
		!kubernetes.IsSystemNamespace(request.Name) &&
		h.usernameFilterFunc(request.UserInfo.Username)
}
