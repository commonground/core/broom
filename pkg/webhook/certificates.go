package webhook

import (
	"encoding/json"
	"fmt"

	admissionv1 "k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/jsonpatch"
)

func certificatesHandler(review *admissionv1.AdmissionReview) (*admissionv1.AdmissionReview, error) {
	response := &admissionv1.AdmissionResponse{UID: review.Request.UID, Allowed: true}

	klog.Infof("certificate '%s' found", review.Request.Name)

	if review.Request.Kind.Kind == "Certificate" &&
		(review.Request.Operation == admissionv1.Create || review.Request.Operation == admissionv1.Update) {
		var object map[string]any

		if err := json.Unmarshal(review.Request.Object.Raw, &object); err != nil {
			return nil, fmt.Errorf("failed to unmarshal object: %w", err)
		}

		var revisionHistoryLimit *int

		if specVal, ok := object["spec"]; ok {
			if spec, ok := specVal.(map[string]any); ok {
				if limit, ok := spec["revisionHistoryLimit"].(int); ok {
					revisionHistoryLimit = &limit
				}
			}
		}

		if revisionHistoryLimit == nil {
			klog.Infof("adding 'revisionHistoryLimit=10' to certificate: %s", review.Request.Name)

			patch := jsonpatch.Patch{}
			patch.Add(
				"add",
				"/spec/revisionHistoryLimit",
				3,
			)

			response.Patch = patch.JSON()
			response.PatchType = func() *admissionv1.PatchType {
				pt := admissionv1.PatchTypeJSONPatch
				return &pt
			}()
		}
	}

	return &admissionv1.AdmissionReview{
		TypeMeta: metav1.TypeMeta{
			Kind:       review.Kind,
			APIVersion: review.APIVersion,
		},
		Response: response,
	}, nil
}
