package webhook

import (
	"net/http"
	"time"

	"gitlab.com/commonground/core/broom/pkg/tls"
)

type HealthServer struct {
	http *http.Server
}

func (s *HealthServer) Run() error {
	return s.http.ListenAndServe()
}

func NewHealthServer(address string, cert *tls.Certificate) *HealthServer {
	mux := http.NewServeMux()
	mux.Handle("/", okHandler())

	if cert != nil {
		mux.Handle("/live", livehHandler(cert))
		mux.Handle("/health", healthHandler(cert))
	}

	server := &HealthServer{
		http: &http.Server{
			Addr:    address,
			Handler: mux,

			ReadHeaderTimeout: 5 * time.Second,
		},
	}

	return server
}

func livehHandler(cert *tls.Certificate) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if !cert.Healthy() {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

func healthHandler(cert *tls.Certificate) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if cert.Expired() {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
	})
}

func okHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
}
