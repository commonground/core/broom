package webhook

import (
	"net/http"
	"time"

	admisstionv1 "k8s.io/api/admission/v1"
	corev1 "k8s.io/api/core/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/tls"
	"gitlab.com/commonground/core/broom/pkg/util"
)

const (
	AnnotationKey    = "broom.commonground.nl/cleanup-after"
	AnnotationFormat = time.RFC3339
)

type Server struct {
	cert *tls.Certificate
	http *http.Server
}

func NewServer(address string, cert *tls.Certificate, duration time.Duration, usernames util.StringList) *Server {
	registerAPIs()

	nsHandler := newNamespacesHandler(duration, usernames)

	mux := http.NewServeMux()
	mux.Handle("/namespaces/mutate", wrapHandler(nsHandler.Handler))
	mux.Handle("/certificates/mutate", wrapHandler(certificatesHandler))

	server := &Server{
		cert: cert,
		http: &http.Server{
			Addr:    address,
			Handler: mux,

			ReadHeaderTimeout: 5 * time.Second,
		},
	}

	if cert != nil {
		server.http.TLSConfig = cert.TLSConfig()
	}

	return server
}

func (s *Server) Run() error {
	address := s.http.Addr

	if s.cert == nil {
		klog.Infof("Listening without TLS on %s... ", address)
		return s.http.ListenAndServe()
	}

	klog.Infof("Listening on %s...", address)
	return s.http.ListenAndServeTLS("", "")
}

func registerAPIs() {
	utilruntime.Must(corev1.AddToScheme(scheme))
	utilruntime.Must(admisstionv1.AddToScheme(scheme))
}
