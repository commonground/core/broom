package webhook

import "gitlab.com/commonground/core/broom/pkg/util"

type UsernameFilterFunc func(username string) bool

func UsernameFilter(usernames util.StringList) UsernameFilterFunc {
	return func(username string) bool {
		return usernames.Contains(username)
	}
}

func NoopFilter() UsernameFilterFunc {
	return func(_ string) bool {
		return true
	}
}
