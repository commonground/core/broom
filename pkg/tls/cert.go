package tls

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"k8s.io/klog/v2"
)

const (
	reloadInterval = 2 * time.Second
	maxFailures    = 3
)

type Certificate struct {
	cert     *tls.Certificate
	certPath string
	keyPath  string

	mu sync.RWMutex

	watcher  *fsnotify.Watcher
	watching bool
}

func NewCertificte(certPath, keyPath string) (*Certificate, error) {
	cert, err := loadCertificate(certPath, keyPath)
	if err != nil {
		return nil, err
	}

	c := &Certificate{
		cert:     cert,
		certPath: certPath,
		keyPath:  keyPath,
		mu:       sync.RWMutex{},
	}

	return c, nil
}

func (c *Certificate) TLSConfig() *tls.Config {
	return &tls.Config{
		MinVersion:     tls.VersionTLS13,
		GetCertificate: c.getCertificate,
	}
}

func (c *Certificate) Expired() bool {
	return time.Now().After(c.cert.Leaf.NotAfter)
}

func (c *Certificate) Healthy() bool {
	return c.watching
}

func (c *Certificate) getCertificate(*tls.ClientHelloInfo) (*tls.Certificate, error) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.cert, nil
}

func (c *Certificate) Watch(ctx context.Context) error {
	if err := c.newWatcher(); err != nil {
		return err
	}

	go c.run(ctx)

	return nil
}

func (c *Certificate) newWatcher() error {
	var err error
	c.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return err
	}

	return c.watcher.Add(c.certPath)
}

func (c *Certificate) resetWatcher() error {
	if err := c.watcher.Close(); err != nil {
		return err
	}

	return c.newWatcher()
}

func (c *Certificate) run(ctx context.Context) {
	c.watching = true

	for {
		select {
		case event, ok := <-c.watcher.Events:
			if !ok {
				return
			}

			if event.Op&fsnotify.Chmod == fsnotify.Chmod {
				// The Kubelet keeps remounting the secret volume, causing this Op.
				continue
			}

			c.watching = false

			klog.Info("Reloading certificate")
			if err := c.reloadCertificate(); err != nil {
				klog.Errorf("Failed to reload certificate: %v", err)
				return
			}

			if err := c.resetWatcher(); err != nil {
				klog.Errorf("Failed to reset watcher: %v", err)
				return
			}

			c.watching = true
			klog.Info("Watcher reset")
		case err := <-c.watcher.Errors:
			klog.Error(err)
		case <-ctx.Done():
			c.watcher.Close()
			return
		}
	}
}

func (c *Certificate) reloadCertificate() error {
	failures := 0
	ticker := time.NewTicker(reloadInterval)
	defer ticker.Stop()

	var cert *tls.Certificate
	var err error

	for {
		<-ticker.C

		if cert, err = loadCertificate(c.certPath, c.keyPath); err != nil {
			failures++

			if failures >= maxFailures {
				return err
			}

			klog.Errorf("Failed to load certificate, retrying: %v", err)

			continue
		}

		break
	}

	c.mu.Lock()
	c.cert = cert
	c.mu.Unlock()

	return nil
}

func loadCertificate(certPath, keyPath string) (*tls.Certificate, error) {
	cert, err := tls.LoadX509KeyPair(certPath, keyPath)
	if err != nil {
		return nil, err
	}

	cert.Leaf, _ = x509.ParseCertificate(cert.Certificate[0])

	klog.Info("Certificate loaded")
	klog.Infof("Certificate not valid before: %s", cert.Leaf.NotBefore)
	klog.Infof("Certificate not valid after: %s", cert.Leaf.NotAfter)

	return &cert, nil
}
