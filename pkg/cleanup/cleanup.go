package cleanup

import (
	"context"
	"os"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/kubernetes"
	"gitlab.com/commonground/core/broom/pkg/util"
	"gitlab.com/commonground/core/broom/pkg/webhook"
)

func Run(config *rest.Config, protectedNamespaces util.StringList, dryRun bool) bool {
	if dryRun {
		klog.Info("Dry run mode")
	}

	runningNamespace := os.Getenv("POD_NAMESPACE")

	klog.Infof("Running in namespace: %s", runningNamespace)
	klog.Infof("Protected namespaces: %s", protectedNamespaces)

	coreV1Client := corev1.NewForConfigOrDie(config)
	ctx := context.Background()

	namespaceList, err := coreV1Client.Namespaces().List(ctx, metav1.ListOptions{})
	if err != nil {
		klog.Errorf("Failed to list namespaces: %v", err)
		return false
	}

	toClean := make([]string, 0, namespaceList.Size())
	now := time.Now()

	for i := range namespaceList.Items {
		namespace := namespaceList.Items[i]

		// Don't delete the namespace we're running in
		if runningNamespace == namespace.Name {
			continue
		}

		if kubernetes.IsSystemNamespace(namespace.Name) || protectedNamespaces.Contains(namespace.Name) {
			continue
		}

		annotationValue, found := namespace.Annotations[webhook.AnnotationKey]
		if !found {
			continue
		}

		cleanUpAfter, err := time.Parse(webhook.AnnotationFormat, annotationValue)
		if err != nil {
			klog.Errorf("Failed to parse annotation on namespace '%s': %v", namespace.Name, err)
			continue
		}

		if now.After(cleanUpAfter) {
			toClean = append(toClean, namespace.Name)
		}
	}

	deleteOpts := metav1.DeleteOptions{}

	if dryRun {
		deleteOpts.DryRun = []string{"All"}
	}

	klog.Infof("Namespaces to clean: %d	", len(toClean))

	for _, name := range toClean {
		klog.Infof("Deleting namespace '%s'...", name)
		if err := coreV1Client.Namespaces().Delete(ctx, name, deleteOpts); err != nil {
			klog.Errorf("Failed to delete namespace: %v", err)
		}
	}

	return true
}
