package kubernetes

import "strings"

func IsSystemNamespace(name string) bool {
	return name == "default" || strings.HasPrefix(name, "kube-")
}
