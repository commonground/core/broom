package main

import (
	"context"
	"errors"
	"flag"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"golang.org/x/sync/errgroup"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/tls"
	"gitlab.com/commonground/core/broom/pkg/util"
	"gitlab.com/commonground/core/broom/pkg/webhook"
)

const (
	podTLSDir = "/var/run/secrets/tls"
	devTLSDir = "tmp"

	defaultDuration = 5 * time.Minute
)

type webhookArgs struct {
	duration      time.Duration
	usernames     listFlag
	listenAddress string
	healthAddress string
	withoutTLS    bool
}

func (a *webhookArgs) AddFlags(f *flag.FlagSet) {
	f.DurationVar(&a.duration, "duration", defaultDuration, "Duration of a namespace")
	f.Var(&a.usernames, "usernames", "Comma separated list of usernames of which the namespace will be cleaned")
	f.StringVar(&a.listenAddress, "listen-address", "127.0.0.1:8443", "Listen address")
	f.StringVar(&a.healthAddress, "health-address", "127.0.0.1:8080", "Listen address for health checks")
	f.BoolVar(&a.withoutTLS, "without-tls", false, "Start server without TLS")
}

func runWebhook(args webhookArgs) bool {
	g, ctx := errgroup.WithContext(context.Background())

	var cert *tls.Certificate
	if !args.withoutTLS {
		var err error

		if cert, err = newCertificate(); err != nil {
			klog.Errorf("Failed to load certificate: %v", err)
			return false
		}

		if err = cert.Watch(ctx); err != nil {
			klog.Errorf("Failed to watch certificate: %v", err)
			return false
		}
	}

	webhookServer := webhook.NewServer(args.listenAddress, cert, args.duration, util.StringList(args.usernames))
	healthServer := webhook.NewHealthServer(args.healthAddress, cert)

	g.Go(webhookServer.Run)
	g.Go(healthServer.Run)

	if err := g.Wait(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		klog.Errorf("Failed to run webhook server: %v", err)
		return false
	}

	return true
}

func newCertificate() (*tls.Certificate, error) {
	tlsDir := podTLSDir

	if _, ok := os.LookupEnv("KUBERNETES_SERVICE_HOST"); !ok {
		tlsDir = devTLSDir
	}

	certPath := filepath.Join(tlsDir, "tls.crt")
	keyPath := filepath.Join(tlsDir, "tls.key")

	return tls.NewCertificte(certPath, keyPath)
}
