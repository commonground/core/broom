package main

import (
	"flag"
	"os"
	"strings"

	"k8s.io/klog/v2"
)

func main() {
	webhookCommand := flag.NewFlagSet("webhook", flag.ExitOnError)
	webhookArgs := webhookArgs{}
	webhookArgs.AddFlags(webhookCommand)

	cleanUpCommand := flag.NewFlagSet("cleanup", flag.ExitOnError)
	cleanUpArgs := cleanUpArgs{}
	cleanUpArgs.AddFlags(cleanUpCommand)

	flag.Parse()

	if len(os.Args) < 2 {
		klog.Error("no command provided")
		os.Exit(2)
	}

	switch os.Args[1] {
	case "webhook":
		if err := webhookCommand.Parse(os.Args[2:]); err != nil {
			klog.Errorf("Failed to parse flags: %v", err)
			os.Exit(1)
			return
		}

		if ok := runWebhook(webhookArgs); !ok {
			os.Exit(1)
		}
	case "cleanup":
		if err := cleanUpCommand.Parse(os.Args[2:]); err != nil {
			klog.Errorf("Failed to parse flags: %v", err)
			os.Exit(2)
			return
		}

		if ok := runCleanUp(cleanUpArgs); !ok {
			os.Exit(1)
		}
	default:
		klog.Error("unknown command")
		os.Exit(2)
	}
}

type listFlag []string

func (l *listFlag) String() string {
	return strings.Join(*l, ",")
}

func (l *listFlag) Set(s string) error {
	if len(s) > 0 {
		*l = strings.Split(s, ",")
	}
	return nil
}
