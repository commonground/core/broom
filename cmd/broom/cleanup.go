package main

import (
	"errors"
	"flag"
	"path/filepath"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"k8s.io/klog/v2"

	"gitlab.com/commonground/core/broom/pkg/cleanup"
	"gitlab.com/commonground/core/broom/pkg/util"
)

type cleanUpArgs struct {
	protectedNamespaces listFlag
	dryRun              bool
}

func (a *cleanUpArgs) AddFlags(f *flag.FlagSet) {
	f.Var(&a.protectedNamespaces, "protected-namespaces", "Comma separated list of namespaces which never be deleted")
	f.BoolVar(&a.dryRun, "dry-run", false, "Don't actually delete namespaces")
}

func runCleanUp(args cleanUpArgs) bool {
	config, err := newConfig()
	if err != nil {
		klog.Error(err)
		return false
	}

	return cleanup.Run(config, util.StringList(args.protectedNamespaces), args.dryRun)
}

func newConfig() (*rest.Config, error) {
	config, err := rest.InClusterConfig()

	if errors.Is(err, rest.ErrNotInCluster) {
		if home := homedir.HomeDir(); home != "" {
			kubeconfigPath := filepath.Join(home, ".kube", "config")
			return clientcmd.BuildConfigFromFlags("", kubeconfigPath)
		}
	}

	return config, err
}
